var Squares = (function() {
	// helpers
	////////////////////////////////////////////////////////////////////////////////////
	var $field = document.getElementById('field'),	// main container
		$generateClicks = document.getElementsByClassName('js-generate-click')[0],
		$showResult = document.getElementsByClassName('js-show-result')[0],
		$resetClicks = document.getElementsByClassName('js-reset-clicks')[0],
		$cells;	// generated cells
	
	// constants
	////////////////////////////////////////////////////////////////////////////////////
	/**
	* dimension {object}
	* number of squares in rows and columns
	*/
	var dimension = {
		rows: 10,
		columns: 10
	};
	
	/**
	* clicks number {integer}
	* number of clicks to generate
	*/
	var clicksNumber = 100;
	
	/**
	* backgrounds {array}
	* list of colors (or other background value), used for cells background, by clicks range
	* range:
	* value > from && value < to
	*/
	var backgrounds = [
		{'from':  0, 'to': 25, 'value': '#FFF'},
		{'from': 26, 'to': 50, 'value': '#FCF6A9'},
		{'from': 51, 'to': 75, 'value': '#FCCF05'},
		{'from': 76, 'to':100, 'value': '#FC8505'},
		{'from':101,           'value': '#F50202'}
	];
	
	// methods
	////////////////////////////////////////////////////////////////////////////////////
	
	var increaseClickCounter = function(square){
		var oldValueOfCounter = parseInt( square.children[0].dataset.counter );
		square.children[0].dataset.counter = oldValueOfCounter + 1;
	}
	
	/**
	* add click on one cell after "mouseclick" {function}
	* @returns
	*/
	var addClick = function(square){
		increaseClickCounter(square);
		var clicksCounterValue = parseInt(square.children[0].dataset.counter);
		square.children[0].innerHTML = clicksCounterValue;
		var newSquareBackground = getNewSquareBackground(clicksCounterValue);
		square.style.background = newSquareBackground;
	};
	
	/**
	* create cell element {function}
	* @returns
	*/
	var createCellInField = function(){
		// create square
		var $square = document.createElement('div');
		// add class to square element
		$square.classList.add('square');
		// create clicks counter container
		var $numberContainer = document.createElement('div');
		// add class to counter container
		$numberContainer.classList.add('click-number');
		// initialize counter
		$numberContainer.dataset.counter = 0;
		// add event listener on square
		$square.addEventListener('click', function(){addClick($square)}, false);
		// add counter container into square
		$square.appendChild($numberContainer);
		// add square into field
		$field.appendChild($square);
	};
	
	/**
	* generate cells by dimension {function}
	* @returns
	*/
	var generateCells = function(){
		// clear field
		$field.innerHTML = '';
		for(var i=0; i<dimension.rows; i++){
			// create cells in line
			for(var j=0; j<dimension.columns; j++){
				createCellInField();
			}
			// add new line
			var $linebreak = document.createElement('br');
			$field.appendChild($linebreak);
		}
		// save cells to variable
		$cells = document.getElementsByClassName('square');
	};
	 
	/**
	* generate clicks in squares {function}
	* @returns
	*/
	var generateClicks = function(){
		var cellsNumber = $cells.length;
		for( var i = 0; i<clicksNumber; i++){
			// get random cell
			var cellIndex = Math.floor((Math.random() * cellsNumber));
			// update cell clicks counter
			increaseClickCounter($cells[cellIndex]);
		}
	};
	
	/**
	* update square background {function}
	* @returns String
	*/
	var getNewSquareBackground = function(clicksCounterValue){
		// initialize variable to return
		var background = '';
		// check clicks conuter in range
		for(var i=0; i<backgrounds.length; i++){
			if( typeof backgrounds[i].from == 'number' ){
				if( clicksCounterValue >= backgrounds[i].from ){
					if( backgrounds[i].to ){
						if( clicksCounterValue <= backgrounds[i].to ){
							background = backgrounds[i].value;
							break;
						}
					} else {
						background = backgrounds[i].value;
						break;
					}
				}
			} else {
				background = backgrounds[i].value;
				break;
			}
		}
		
		return background;
	};
	
	/**
	* display clicks in squares {function}
	* @returns
	*/
	var updateDisplayedCounter = function(){
		for( var i = 0; i<$cells.length; i++){
			var clicksCounterValue = parseInt($cells[i].children[0].dataset.counter);
			$cells[i].children[0].innerHTML = clicksCounterValue;
			var newSquareBackground = getNewSquareBackground(clicksCounterValue);
			$cells[i].style.background = newSquareBackground;
		}
	};

	
	// buttons listeners
	////////////////////////////////////////////////////////////////////////////////////
	var setupButtons = function(){
		$generateClicks.addEventListener('click', generateClicks);
		$showResult.addEventListener('click', updateDisplayedCounter);
		$resetClicks.addEventListener('click', generateCells);
	};
	
	var init = function(){
		generateCells();
		setupButtons();
	};
	
	// return - public
	////////////////////////////////////////////////////////////////////////////////////
	return {
		init: init
	};

})();

Squares.init();